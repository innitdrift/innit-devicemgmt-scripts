#Variables
$wpBlobUrl = "https://innitinternal.blob.core.windows.net/publiclogo/Bakgrunn/Bakgrunnsbilde%202020-2.jpg";
$wpPath = "C:\IntuneIntune\Wallpaper\";
$wpname = "Bakgrunn.jpg"

if (!(test-path $wpPath)) {
    mkdir $wpPath
}

$wpOutFilePath = $wpPath + $wpname;

Invoke-WebRequest -Uri $wpBlobUrl -OutFile $wpOutFilePath

Function Set-Wallpaper($value) {
 Set-ItemProperty -path 'HKCU:\Control Panel\Desktop\' -name wallpaper -value $value

 rundll32.exe user32.dll, UpdatePerUserSystemParameters
}

Set-Wallpaper -value $wpOutFilePath