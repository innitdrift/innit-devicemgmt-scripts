# Copies Office install script to local machine, because fuck N-Central parameter handling.
# Author: Mikael Lognseth @ Innit Drift AS
$InstallFolder = "C:\Innit\"
$ScriptUri = "https://innitdevicemgmt.blob.core.windows.net/innit/Scripts/GenericCustomer_InstallOffice/ODT_InstallOffice.ps1"
$ProgressPreference = "SilentlyContinue"
$InstallPath = $InstallFolder + "InstallOffice.ps1"
$logFolder = 'C:\Innit\00_logs';
$logPath = Join-Path $logFolder "$scriptName.txt"; 



$Minutes = $Sleeptimer -as [int]
$Seconds = $Minutes * 60

Start-Sleep -Seconds $Seconds

Start-Transcript -Path $logPath -Append; 

if (!(Test-Path $InstallFolder)) {
    New-Item -ItemType Directory -Path $InstallFolder;
}

Invoke-WebRequest -Uri $ScriptUri -OutFile $InstallPath

& $InstallPath

Stop-Transcript